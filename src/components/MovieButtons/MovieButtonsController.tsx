import React from "react";
import { navigate } from "gatsby";
import {getLangFromLs} from "../../utils/localstorage";

const useMovieButtonsController = () => {
  const lang = getLangFromLs();

  const handleClick = (id: number) => async () => {
    await navigate(`/${lang}/movie/${id}`);
  };

  return { lang, handleClick };
};

export default useMovieButtonsController;
