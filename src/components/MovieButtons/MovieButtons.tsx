import React from "react";
import movies from "../../data/movies.json";
import Button from "../ui/Button/Button";
import useMovieButtonsController from "./MovieButtonsController";
import ButtonsGroup from "../ui/ButtonsGroup/ButtonsGroup";

const MovieButtons = () => {
  const { handleClick, lang } = useMovieButtonsController();
  return (
    <ButtonsGroup>
      {movies.map(({ id, title }) => (
        <Button
          key={id}
          text={title[lang]}
          click={handleClick(id)}
          full
          primary
        />
      ))}
    </ButtonsGroup>
  );
};

export default MovieButtons;
