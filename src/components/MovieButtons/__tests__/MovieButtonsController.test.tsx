import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";

import { navigate } from 'gatsby';
import useMovieButtonsController from '../MovieButtonsController';

jest.mock('gatsby', () => ({
  navigate: jest.fn(),
}));

jest.mock('../../../utils/localstorage', () => ({
  getLangFromLs: jest.fn(() => 'es'),
}));

const MockComponent = () => {
  const { handleClick } = useMovieButtonsController();
  return (
    <button onClick={handleClick(1)}>Fake text</button>
  );
};

describe('useMovieButtonsController', () => {
  it('should navigate to the correct URL on button click', async () => {
    const { getByText } = render(<MockComponent />)

    fireEvent.click(getByText('Fake text'));

    expect(navigate).toHaveBeenCalledWith('/es/movie/1');
  });
});


