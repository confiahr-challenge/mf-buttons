import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import MovieButtons from "../MovieButtons";
import MovieButtonsController from "../MovieButtonsController";
import useMovieButtonsController from "../MovieButtonsController";

jest.mock("../MovieButtonsController");

describe("MovieButtons Component", () => {
  it("renders correctly", () => {
    (useMovieButtonsController as jest.Mock).mockImplementation(() => ({
      handleClick: jest.fn(),
      lang: "en",
    }));
    const { container } = render(<MovieButtons />);
    expect(container).toMatchSnapshot();
  });

  it("calls handleClick in EN when a button is clicked", () => {
    (useMovieButtonsController as jest.Mock).mockImplementation(() => ({
      handleClick: jest.fn(),
      lang: "en",
    }));

    const { getByText } = render(<MovieButtons />);
    const button = getByText(/Avengers/i);
    fireEvent.click(button);
  });

  it("calls handleClick in ES when a button is clicked", () => {
    (useMovieButtonsController as jest.Mock).mockImplementation(() => ({
      handleClick: jest.fn(),
      lang: "es",
    }));

    const { getByText } = render(<MovieButtons />);
    const button = getByText(/Los vengadores/i); // Ajusta el texto según tu implementación
    fireEvent.click(button);
  });
});
