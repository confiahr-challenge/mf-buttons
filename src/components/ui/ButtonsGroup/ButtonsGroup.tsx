import React, { PropsWithChildren } from "react";
import { ButtonsGroupStyles } from "./ButtonsGroupStyles";

const ButtonsGroup: React.FC<PropsWithChildren> = ({ children }) => {
  return <ButtonsGroupStyles>{children}</ButtonsGroupStyles>;
};

export default ButtonsGroup;
