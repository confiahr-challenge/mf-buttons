import styled from "styled-components";

export const ButtonsGroupStyles = styled.div`
  display: flex;
  gap: 1rem;
`;
