import React from "react";
import { ButtonStyled } from "./ButtonStyled";

interface Props {
  full?: boolean;
  primary?: boolean;
  click: () => void;
  text: string;
}

const Button: React.FC<Props> = ({ full, primary, click, text }) => {
  return (
    <ButtonStyled $full={full} $primary={primary} onClick={click}>
      {text}
    </ButtonStyled>
  );
};

export default Button;
