import styled, { css } from "styled-components";

interface Props {
  $full?: boolean;
  $primary?: boolean;
}

const primaryCss = css`
  background: #356be9;
  color: #fff;
`;

export const ButtonStyled = styled.button<Props>`
  ${({ $full }) => $full && "width: 100%"};
  ${({ $primary }) => $primary && primaryCss};

  font-family: "Manrope", serif;
  border-radius: 4px;
  border: none;
  padding: 16px 10px;
  text-transform: uppercase;
  letter-spacing: 0.17em;
  cursor: pointer;
`;
