import { ILangCode } from "../interfaces/ILangCode";

export const getLangFromLs = (): ILangCode => {
  if (typeof window !== "undefined") {
    return localStorage.getItem("lang") as ILangCode;
  }
  return "en";
};
