import type { GatsbyConfig } from "gatsby";

const config: GatsbyConfig = {
  siteMetadata: {
    title: `mf-series-b`,
    siteUrl: `https://www.yourdomain.tld`,
  },
  // More easily incorporate content into your pages through automatic TypeScript type generation and better GraphQL IntelliSense.
  // If you use VSCode you can also use the GraphQL plugin
  // Learn more at: https://gatsby.dev/graphql-typegen
  graphqlTypegen: true,
  plugins: [
    "gatsby-plugin-styled-components",
    {
      resolve: "gatsby-plugin-federation",
      options: {
        ssr: false,
        federationConfig: {
          name: "mfButtons",
          exposes: {
            "./App": "./src/components/App",
          },
          shared: {
            "styled-components": {
              singleton: true,
            },
          },
        },
      },
    },
  ],
};

export default config;
